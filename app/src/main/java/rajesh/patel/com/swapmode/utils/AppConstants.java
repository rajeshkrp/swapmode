package rajesh.patel.com.swapmode.utils;

import android.support.v4.app.Fragment;

public class AppConstants extends Fragment {
    public static final int MULTI_RESPONSE_CODE = 11;
    public static  final String IS_LOGIN = "false";
    public static  final String FIRST_TIME_TUTORIAL = "first_time_login";
    public static  final String ALL_USER = "all_user";
    public static  final String GUESR_USER = "guest_user";
    public static  final String COUNTRY_LIST = "false";
    public static  final String PLUMBER_USER_ID = "plumber";
    public static  final String CITY_POSITION = "position";

    public static final String PLUMBER_REQUEST_SERVICE="request";
    public static final String REQUEST_DETAILS="RequestDetails";
    public static  final String FORGOT_PASSWORD = "true";
    public static  final String PRIVACY = "true";
    public static  final String FORGOT_EMAIL_NUMBER = "email";
    public final   static Boolean background = false;
    public static final String WORK_IN="2";
    public  static final String distributor_login="distributerKey";
    public  static final String dealer_login ="dealerKey";
    public  static final String DEALER_LOGIN ="dealerKey";
    public  static final String DISTRIBUTER_LOGIN ="dealerKey";

    public final static String CATEGORY ="category";
    public final static String SUB_CATEGORY ="sub_category";
    public final static String PRODUCT ="product";
    public final static String SCREEN ="screen";
    public final static String PLUMBER_LIST_KEY ="plumbeList";
    public final static String CURRENT_LOCATION="current_location";




    public static  final String FIREBASE_KEY = "fire_base";
    public static  final String REQUEST_CHAT_KEY = "true";
    public static  final String UNREAD_MESSAGE_KEY = "request_chat";
    public static  final String REQUEST_MESSAGE_KEY = "message_chat";
    public static  final String NUMBER_OF_REQUEST_KEY = "request_chat";
    public static  final String CONTACt_EMail_KEY = "email";
    public static  final String CONTACt_EMail_KEY_panding = "emailpanding";

    public static  final String COUNT_KEY = "count";
    public static  final String PLUMBER_STATUS = "plumber_sataus";
    public static  final String SORT_BY_KEY = "sort";

    public static  final String KEY_FILTER = "false";
    public static  final String KEY_OTP = "otp";

    public static final String FILTER_DATA = "filter_data";



    public static final String USER_EMAIL = "email";


    public static final String SELECTED_CITY_CURRENT = "selected_city_current";
    public static final String CURRENT_LAT = "current_lat";
    public static final String USER_ID = "user_id";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String DRAWER_PIC = "drawer_pic";
    public static final String MOBILE_NO = "mobile_no";
    public static final String Registerd_MOBILE_NO = "mobile_no";
    public static final String PROFILE_PERCENTAGE = "profile_percentage";
    public static final String USER_NAME = "user_name";
    public static final String SENDER_USER_NAME = "sender_name";
    public static final String SENDER_PROFILE_PIC = "profile_name";

    public static final String CURRENT_LONGI = "current_longi";
    public static String LAT = "lat";
    public static String LONGI = "longi";
    public static String COMPLAIN_ID = "complain_id";
    public static final String CurrentCity="city_name";
    public static final String StateName ="country_name";
  //  public static final String HASH_KEY ="hash_key";
    public static final String HASHTAG_KEY = "hashtag";
    public static final String ALL_HASHTAG_KEY = "hashtagkey";
    public static final String NOTIFICATION_KEY = "notification";
    public static final String FOLLOWED_POST_KEY = "followed_post";


    public static final int FRAGMENT_HOME=0;
    public static final int FRAGMENT_NOTIFICATION=1;
    public static final int FRAGMENT_ALL=2;


    public static final int FRAGMENT_POSTS=0;
    public static final int FRAGMENT_POLLS=1;
    public static final int FRAGMENT_FOLLOED_POSTS=2;
    public static final int FRAGMENT_FOLLOWED_POLLS=3;
    public static final int FRAGMENT_COMMENT_REPLY=4;



    public static class SharedPrefKeys {
        public static final String MY_SHAREDPREF_NAME = "com.netwrko";
        public static final String contentType = "application/json";



    }

}
