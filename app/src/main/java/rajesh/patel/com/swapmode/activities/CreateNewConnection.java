package rajesh.patel.com.swapmode.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.adapters.TagListAdapter;
import rajesh.patel.com.swapmode.model.TagModel;
import rajesh.patel.com.swapmode.utils.CommonUtils;

public class CreateNewConnection extends AppCompatActivity implements View.OnClickListener {

    TextView tv_save;
    ImageView iv_back;
    EditText et_f_name,et_role,et_email,et_mobile,et_address;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Context context;
    String email="";
    Button btn_bottom_sheet;
    RelativeLayout bottom_sheet;
    LinearLayout ll_add_tag;
    TagListAdapter tagListAdapter;
    RelativeLayout layoutBottomSheet;
    ArrayList<TagModel> tagList;
    TagModel tagModel;
    BottomSheetBehavior sheetBehavior;
    RecyclerView recy_tag_container,recy_tag_list;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_conncetion);
        context=CreateNewConnection.this;
        tv_save=findViewById(R.id.tv_save);
        iv_back=findViewById(R.id.iv_back);
        et_address=findViewById(R.id.et_address);
        et_f_name=findViewById(R.id.et_f_name);
        et_role=findViewById(R.id.et_role);
        et_email=findViewById(R.id.et_email);
        et_mobile=findViewById(R.id.et_mobile);
        bottom_sheet=findViewById(R.id.bottom_sheet);
        ll_add_tag=findViewById(R.id.ll_add_tag);
        layoutBottomSheet=findViewById(R.id.bottom_sheet);
        recy_tag_list=findViewById(R.id.recy_tag_list);
        recy_tag_container=findViewById(R.id.recy_tag_container);



        tv_save.setOnClickListener(this);
        iv_back.setOnClickListener(this);


        ll_add_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                    tagListData();





                    //  btnBottomSheet.setText("Close sheet");
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                  //  btnBottomSheet.setText("Expand sheet");
                }

            }
        });

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                       // btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                      //  btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void tagListData() {

        tagList=new ArrayList<>();
        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C++");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);


        tagListAdapter=new TagListAdapter(tagList,context);
        recy_tag_list.setLayoutManager(new LinearLayoutManager(context));
        recy_tag_list.setAdapter(tagListAdapter);



    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_save:
                validation();
                break;

            case R.id.iv_back:
                onBackPressed();
            break;
        }
    }

    private void validation() {
        String fullname = et_f_name.getText().toString();
        email=et_email.getText().toString();


        if (TextUtils.isEmpty(et_f_name.getText().toString())) {
            CommonUtils.snackBar("  Please Enter First Name.", et_f_name);
        } else if (!fullname.matches("[a-zA-Z ]+")) {
            CommonUtils.snackBar("Name Accept Alphabets Only.", et_f_name);
        }
        else if (TextUtils.isEmpty(et_email.getText())) {

            CommonUtils.snackBar("Please Enter Your email.", et_f_name);
        }else if(!email.matches(emailPattern)&&email.length()>0){
            CommonUtils.snackBar("Please Enter Right Email Address.", et_f_name);

        }

        else if (TextUtils.isEmpty(et_mobile.getText().toString())) {
            CommonUtils.snackBar("Please Enter Phone Number.", et_f_name);
        }
        else if (et_mobile.getText().toString().length() < 10) {
            CommonUtils.snackBar("Mobile Number Should Be of 10 Digits.", et_f_name);
        }
        else if (et_mobile.getText().toString().length() > 10) {
            CommonUtils.snackBar("Mobile Number Should Be of 10 Digits.", et_f_name);
        }
        else if (TextUtils.isEmpty(et_role.getText().toString())) {

            CommonUtils.snackBar("Please Enter Your Role or Description.", et_f_name);
        }


        else if (TextUtils.isEmpty(et_address.getText().toString())) {
            CommonUtils.snackBar("Please Enter Your Address.", et_f_name);


        }
        else {


            et_address.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tv_save.setTextColor(context.getResources().getColor(R.color.orange));
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    tv_save.setTextColor(context.getResources().getColor(R.color.orange));

                    // TODO Auto-generated method stub
                }
            });


        }

//            callSignUApi();

        }
    }


