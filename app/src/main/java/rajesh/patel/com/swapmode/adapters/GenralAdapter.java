package rajesh.patel.com.swapmode.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.model.GenralModel;

/**
 * Created by user on 2/16/2018.
 */

public class GenralAdapter extends RecyclerView.Adapter<GenralAdapter.ViewHolder> {

    GenralModel dealerListModel;
    ArrayList<GenralModel> arrayList = new ArrayList<>();

    Context context;

    public GenralAdapter(ArrayList<GenralModel> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public GenralAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_genral_row_item, parent, false);

        GenralAdapter.ViewHolder recyclerViewHolder = new GenralAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(GenralAdapter.ViewHolder holder, int position) {

      /*  holder.repeateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, RepeatOrder.class));
            }
        });
*/
    /*    if (position % 6 == 0) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#4B0082"));

        } else if (position % 6 == 1) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FFA500"));

        } else if (position % 6 == 2) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#48D1CC"));

        } else if (position % 6 == 3) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#C71585"));

        } else if (position % 6 == 4) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#00FFFF"));

        } else if (position % 6 == 5) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FFD700"));

        } else if (position % 6 == 6) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FF0000"));

        }
        if (position == 0) {
            holder.contentRelative.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        if (position % 2 == 0) {

            holder.contentRelative.setBackgroundColor(Color.parseColor("#eeeeee"));
        } else {
            holder.contentRelative.setBackgroundColor(Color.parseColor("#fffefefe"));
        }
*/



        //  holder.pro_image2.setImageResource(arrayList.get(position).getImgID());

        holder.tag_name.setText(arrayList.get(position).getName());

        // holder.pro_name2.setText(arrayList.get(position).getProductName());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tag_name;

        public ViewHolder(View itemView) {
            super(itemView);

            tag_name = itemView.findViewById(R.id.tv_tag_name);

        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter((color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
