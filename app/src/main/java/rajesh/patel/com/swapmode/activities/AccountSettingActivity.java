package rajesh.patel.com.swapmode.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.adapters.GenralAdapter;
import rajesh.patel.com.swapmode.model.GenralModel;
import rajesh.patel.com.swapmode.utils.CommonUtils;

public class AccountSettingActivity extends AppCompatActivity implements View.OnClickListener {

    GenralAdapter genralAdapter;
    GenralModel model;
    ArrayList<GenralModel> arrayList;
    RecyclerView recyclerView;
    Context context;
    ImageView civ_pro_pic_update,iv_back;
    CircleImageView civ_user_pic;
    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String picturePath = "";
    Bitmap bitmap;
    LinearLayout ll_create_new_tag;

    TextView tv_genral,tv_tags;
    FrameLayout frame_tags,frame_genral;


    String mCurrentPhotoPath="";
    String imageClick="";

    private ArrayList<String> imagelist=new ArrayList<>();
    private  String profile_pic="";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acc_setting);
        recyclerView=findViewById(R.id.recy_genral);
        context=AccountSettingActivity.this;
        civ_pro_pic_update=findViewById(R.id.civ_pro_pic_update);
        frame_genral=findViewById(R.id.frame_genral);
        frame_tags=findViewById(R.id.frame_tags);
        tv_genral=findViewById(R.id.tv_genral);
        tv_tags=findViewById(R.id.tv_tags);


        civ_user_pic=findViewById(R.id.civ_user_pic);
        ll_create_new_tag=findViewById(R.id.ll_create_new_tag);
        iv_back=findViewById(R.id.iv_back);

        tv_tags.setOnClickListener(this);
        tv_genral.setOnClickListener(this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        civ_pro_pic_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraGalleryDialog();
            }
        });

        ll_create_new_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTagDialog();
            }
        });



        arrayList=new ArrayList<>();
        for(int i=0;i<10;i++){
            model=new GenralModel();
            model.setName("java");
            arrayList.add(model);

        }
        Log.e("LIST_DATA", String.valueOf(arrayList.size()));
        genralAdapter=new GenralAdapter(arrayList,context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(genralAdapter);

    }

    private void createTagDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window=((Activity) context).getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
            // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
            int height = WindowManager.LayoutParams.WRAP_CONTENT;
            // dialog.getWindow().setLayout(width, height);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_create_new_tag);
            dialog.show();

            TextView tv_create=dialog.findViewById(R.id.tv_create);
        tv_create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                   /* Intent intent=new Intent(mContext,ChangePasswordActivity.class);
                    intent  .putExtra("StringName", password);
                    startActivity(intent);*/
                }
            });
            TextView tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                   /* startActivity(new Intent(mContext, DashBoardActivity.class));
                    finish();*/
                }
            });






    }


    public void openCameraGalleryDialog() {
        final CharSequence[] items = {"Open Camera", "Open Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(
                this);
        builder.setTitle("Select : ");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera")) {
                    cameraPermissionMethod();
                } else if (items[item].equals("Open Gallery")) {
                    gallleryPermissionMethod();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraPermissionMethod() {


        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            imageClick="Camera";
            activityForCamera();
        }}private void gallleryPermissionMethod() {
        imageClick="Gallery";

        if (requestPermission(GALLERY_REQUEST_CODE, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE})) {
            activityForGallery();
        }
    }
    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(this, permission, requestCode);

        return isAlreadyGranted;

    }
    protected boolean checkPermission(String[] permission){

        boolean isPermission = true;

        for(String s: permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(this,s) == PackageManager.PERMISSION_GRANTED;

        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            onPermissionResult(requestCode, true);

        } else {

            onPermissionResult(requestCode, true);
//            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();

        }

    }

    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry, There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(context, getApplicationContext()
                        .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList = getPackageManager()
                            .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }


    private void activityForGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select : "), REQUEST_GALLERY);
    }

    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


        if (isPermissionGranted) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                activityForCamera();
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                activityForGallery();
            }
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    imagelist.clear();

                    imagelist.add(mCurrentPhotoPath);
                    Log.e("camera",mCurrentPhotoPath);
                    bitmap= CommonUtils.getBitMapFromImageURl(mCurrentPhotoPath, (Activity) context);
                    civ_user_pic.setImageBitmap(bitmap);
                    // profileViewDialog(bitmap);
                 //   civ_user_pic.setImageBitmap(bitmap);
                    //iv_default_adhar.setVisibility(View.GONE);
                    break;
                case REQUEST_GALLERY:
                    imagelist.clear();
                    galleryOperation(data);
                    imagelist.add(picturePath);
                    bitmap=CommonUtils.getBitMapFromImageURl(picturePath, (Activity) context);

                    civ_user_pic.setImageBitmap(bitmap);

                    Log.e("galleryIsha",picturePath);

                    break;

            }
        }
    }

    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }


    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        if (selectedImage == null) {
            Toast.makeText(context, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }
        }


       /* galleryOperation(data);
        imagelist.add(picturePath);
        Bitmap bitmap2=CommonUtils.getBitMapFromImageURl(picturePath, (Activity) context);
        ivAadharcardImage.setImageBitmap(bitmap2);
        Log.e("galleryIsha",picturePath);*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_genral:
                frame_genral.setBackgroundResource(R.drawable.left_uper_round_bg_black_layout);
                frame_tags.setBackgroundResource(R.drawable.right_uper_round_bg_orange_layout);
                tv_tags.setTextColor(getResources().getColor(R.color.black));
                tv_genral.setTextColor(getResources().getColor(R.color.orange_deep));


                break;
            case R.id.tv_tags:
                frame_genral.setBackgroundResource(R.drawable.left_uper_round_bg_orange_layout);
                frame_tags.setBackgroundResource(R.drawable.right_uper_round_bg_black_layout);
                tv_tags.setTextColor(getResources().getColor(R.color.orange_deep));
                tv_genral.setTextColor(getResources().getColor(R.color.black));

                break;


        }


    }
}
