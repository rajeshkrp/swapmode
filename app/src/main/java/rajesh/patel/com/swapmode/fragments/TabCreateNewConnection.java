package rajesh.patel.com.swapmode.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.adapters.TagListAdapter;
import rajesh.patel.com.swapmode.model.TagModel;

public class TabCreateNewConnection extends Fragment {
    RecyclerView rcy_create_new;
    Context context;


    private TagListAdapter mAdapter;
    private ArrayList<TagModel> mSectionList;
    private String[] mCountries;

    TagListAdapter tagListAdapter;
    RelativeLayout layoutBottomSheet;
    ArrayList<TagModel> tagList;
    TagModel tagModel;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_create_new_connection, container, false);
        context=getActivity();
        rcy_create_new=view.findViewById(R.id.rcy_create_new);




        tagListData();




    /*    if (arrayList!= null) {

            if (arrayList.size() > 0) {

                for (ListFriend friend:arrayList) {
                    dataListFriend = friend;

                }

            }
        }
        if(dataListFriend!=null) {

            adapter = new ListFriendsAdapter(context, dataListFriend, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recy_chat_list.setLayoutManager(linearLayoutManager);
            adapter = new ListFriendsAdapter(context, dataListFriend, this);
            recy_chat_list.setAdapter(adapter);
        }*/


        return view;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }



    private void tagListData() {

        tagList=new ArrayList<>();
        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C++");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);


        tagListAdapter=new TagListAdapter(tagList,context);
        // getHeaderListLatter(tagModel);
        rcy_create_new.setHasFixedSize(true);
        rcy_create_new.setLayoutManager(new LinearLayoutManager(context));
        rcy_create_new.setAdapter(tagListAdapter);



    }



}
