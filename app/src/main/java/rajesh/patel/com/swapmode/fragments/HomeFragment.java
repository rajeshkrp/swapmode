package rajesh.patel.com.swapmode.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.activities.AccountSettingActivity;
import rajesh.patel.com.swapmode.activities.CreateNewConnection;

public class HomeFragment extends Fragment implements View.OnClickListener {
    RecyclerView recy_chat_list;
    Context context;
ImageView iv_account;
TextView tv_create_new_connection,tv_capture_new_connection;



FrameLayout frame_frag_home_container;
FrameLayout rl_my_connection;
Fragment fragment;
FragmentTransaction transaction;
FragmentManager manager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_home, container, false);


        tv_create_new_connection=view.findViewById(R.id.tv_create_new_connection);
        tv_capture_new_connection=view.findViewById(R.id.tv_capture_new_connection);
        rl_my_connection=view.findViewById(R.id.rl_my_connection);
       // frame_frag_home_container=view.findViewById(R.id.frame_frag_home_container);
        iv_account=view.findViewById(R.id.iv_account);


        context=getActivity();
        tv_capture_new_connection.setOnClickListener(this);
        tv_create_new_connection.setOnClickListener(this);
        rl_my_connection.setOnClickListener(this);
        iv_account.setOnClickListener(this);




        return view;

    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_create_new_connection:
                getActivity().startActivity(new Intent(getActivity(),CreateNewConnection.class));
                break;

            case R.id.tv_capture_new_connection:


                break;

            case R.id.iv_account:
                getActivity().startActivity(new Intent(getActivity(),AccountSettingActivity.class));

                break;

            case R.id.rl_my_connection:

                fragment=new MyconnectionFragment();
                manager=getChildFragmentManager();
                transaction=manager.beginTransaction();
                transaction.replace(R.id.ll_frag_myconn_container,fragment);
                transaction.commit();



                break;


        }
    }
}
