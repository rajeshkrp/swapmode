package rajesh.patel.com.swapmode.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.activities.AccountSettingActivity;
import rajesh.patel.com.swapmode.adapters.MyConnectionTabAdapter;

public class MyconnectionFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    TextView tv_toolbar;

    ImageView iv_account,iv_my_conn_back;


    RelativeLayout rl_add_new_connetion,    rl_create_new_connetion;


    private int[] tabIcons = {
            R.drawable.ic_camera_alt_black_24dp,
            R.drawable.ic_camera_alt_black_24dp
    };

    private ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_my_connection, container, false);
        context=getActivity();

        rl_add_new_connetion=view.findViewById(R.id.rl_add_new_connetion);
        rl_create_new_connetion=view.findViewById(R.id.rl_create_new_connetion);
        iv_account=view.findViewById(R.id.iv_user);
        iv_my_conn_back=view.findViewById(R.id.iv_my_conn_back);
       /* tv_toolbar=view.findViewById(R.id.tv_toolbar);
        tv_toolbar.setText("My Connections");
        tv_toolbar.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
        tv_toolbar.setTextColor(getResources().getColor(R.color.white));
*/


        rl_create_new_connetion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabAddNewConnection newConnection=new TabAddNewConnection();
                FragmentManager manager=getFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.replace(R.id.connection_container,newConnection);

                transaction.commit();

            }


        });




        rl_add_new_connetion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabCreateNewConnection newConnection=new TabCreateNewConnection();
                FragmentManager manager=getFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.replace(R.id.connection_container,newConnection);
                transaction.commit();

            }


        });

        iv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(),AccountSettingActivity.class));

            }
        });

        iv_my_conn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

/*
        tv_toolbar=view.findViewById(R.id.tv_toolbar);
        TabLayout tabLayout=view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("List View"));
        tabLayout.addTab(tabLayout.newTab().setText("Map View"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

         viewPager = (ViewPager)view.findViewById(R.id.pager);

        tv_toolbar.setText("My Conncetions");
        tv_toolbar.setTextSize(18);

        final MyConnectionTabAdapter adapter = new MyConnectionTabAdapter
                (getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/




        return view;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }


}
