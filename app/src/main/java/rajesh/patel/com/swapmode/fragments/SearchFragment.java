package rajesh.patel.com.swapmode.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.adapters.TagListAdapter;
import rajesh.patel.com.swapmode.model.TagModel;

public class SearchFragment extends Fragment {
    RecyclerView recy_search_tag;
    Context context;
    TextView tv_toolbar;

    TagListAdapter tagListAdapter;
    RelativeLayout layoutBottomSheet;
    ArrayList<TagModel> tagList=new ArrayList<>();
    TagModel tagModel;
    EditText et_search;
    ImageView iv_search_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_search, container, false);

        et_search=view.findViewById(R.id.et_search);

        context=getActivity();
        recy_search_tag=view.findViewById(R.id.recy_search_tag);
        iv_search_back=view.findViewById(R.id.iv_search_back);


        tagListData();

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });


        iv_search_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }



    private void tagListData() {

        tagList=new ArrayList<>();
        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C++");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);


        tagListAdapter=new TagListAdapter(tagList,context);
        recy_search_tag.setLayoutManager(new LinearLayoutManager(context));
        recy_search_tag.setAdapter(tagListAdapter);



    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<TagModel> filterList = new ArrayList();

        //looping through existing elements
        for (TagModel s : tagList) {
            //if the existing elements contains the search input
            if (s.getTagName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        if(filterList!=null&&filterList.size()>0) {
            tagListAdapter.filterList(filterList);
        }
    }


}
