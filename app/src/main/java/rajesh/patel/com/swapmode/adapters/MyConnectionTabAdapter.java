package rajesh.patel.com.swapmode.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rajesh.patel.com.swapmode.fragments.TabAddNewConnection;
import rajesh.patel.com.swapmode.fragments.TabCreateNewConnection;

/**
 * Created by user on 2/15/2018.
 */

public class MyConnectionTabAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public MyConnectionTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
               // TabFragUserSignIn tab1=new TabFragUserSignIn();
                TabAddNewConnection tab1=new TabAddNewConnection();

                return tab1;
            case 1:
               // TabFragmentSignUp tab2 = new TabFragmentSignUp();
                TabCreateNewConnection tab2=new TabCreateNewConnection();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
