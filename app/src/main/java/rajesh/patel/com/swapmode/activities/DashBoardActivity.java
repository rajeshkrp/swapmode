package rajesh.patel.com.swapmode.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.fragments.HomeFragment;
import rajesh.patel.com.swapmode.fragments.MyconnectionFragment;
import rajesh.patel.com.swapmode.fragments.SearchFragment;

public class DashBoardActivity extends AppCompatActivity {

    private TextView mTextMessage;
    FragmentManager manager;
    FragmentTransaction transaction;
    Fragment fragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    showHome();
                    return true;
                case R.id.navigation_dashboard:
                    showMyConnection();

                    return true;
                case R.id.navigation_notifications:
                        showSearch();

                    return true;
            }
            return false;
        }
    };

    private void showSearch() {

        fragment=new SearchFragment();
        manager=getSupportFragmentManager();
        transaction=manager.beginTransaction();
        transaction.replace(R.id.frag_container,fragment);
       // transaction.addToBackStack("Search");
        transaction.commit();



    }

    private void showMyConnection() {
        fragment=new MyconnectionFragment();
        manager=getSupportFragmentManager();
        transaction=manager.beginTransaction();
        transaction.replace(R.id.frag_container,fragment);
       // transaction.addToBackStack("MyConnection");
        transaction.commit();


    }

    private void showHome() {

        fragment=new HomeFragment();
        manager=getSupportFragmentManager();
        transaction=manager.beginTransaction();
        transaction.replace(R.id.frag_container,fragment);
        transaction.commit();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        mTextMessage = (TextView) findViewById(R.id.message);

        fragment=new HomeFragment();
        manager=getSupportFragmentManager();
        transaction=manager.beginTransaction();
        transaction.replace(R.id.frag_container,fragment);
       // transaction.addToBackStack(null);
        transaction.commit();



        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }




    @Override
    public void onBackPressed() {
        /*if (sendBackPressToDrawer()) {
            //the drawer consumed the backpress
            return;
        }

        if (sendBackPressToFragmentOnTop()) {
            // fragment on top consumed the back press
            return;
        }

        //let the android system handle the back press, usually by popping the fragment
        super.onBackPressed();

        //close the activity if back is pressed on the root fragment
        if (fragmentManager.getBackStackEntryCount() == 0) {
            finish();
        }
*/


        if(fragment instanceof MyconnectionFragment){
showHome();

        }else if(fragment instanceof SearchFragment){
            showHome();

        }else {
            showHome();
        }
/*
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            super.onBackPressed();
        }
        // Else removes the latest fragment
        else {
            getSupportFragmentManager().popBackStack();
        }*/
    }
}
