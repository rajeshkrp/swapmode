package rajesh.patel.com.swapmode.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.model.TagModel;

/**
 * Created by Admin on 3/6/2018.
 */

public class TagListAdapter extends RecyclerView.Adapter<TagListAdapter.ViewHolder_latest> {
    FragmentTransaction fragmentTransaction;
    ArrayList<TagModel> arrayList;
    Context context;
    RelativeLayout recyclerView;


    public TagListAdapter(ArrayList<TagModel> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;


    }

    @Override
    public ViewHolder_latest onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_list_row_item, parent, false);




        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              /*  NewsDetailActivity fragment = new NewsDetailActivity();

                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
*/

            }
        });


        return new ViewHolder_latest(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder_latest holder, int position) {

        TagModel latest_pojo = arrayList.get(position);


       /* holder.pic_latest.setImageResource(latest_pojo.pic_latest);
        if (position == 0) {
            holder.txt1_latest.setTextColor(Color.parseColor("#87CEEB"));
            holder.txt1_latest.setText(latest_pojo.getTxt1_latest());
            holder.linear_latest.setBackgroundColor(Color.parseColor("#87CEEB"));
        } else if (position % 2 == 1) {

            holder.txt1_latest.setTextColor(Color.parseColor("#FFA500"));
            holder.txt1_latest.setText(latest_pojo.getTxt1_latest());
            holder.linear_latest.setBackgroundColor(Color.parseColor("#FFA500"));

        } else if (position % 2 == 0) {
            holder.txt1_latest.setTextColor(Color.parseColor("#FFB6C1"));
            holder.txt1_latest.setText(latest_pojo.getTxt1_latest());
            holder.linear_latest.setBackgroundColor(Color.parseColor("#FFB6C1"));

        }
*/
        holder.tv_tag_name.setText(latest_pojo.getTagName());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder_latest extends RecyclerView.ViewHolder {
        ImageView iv_selected_row;
        TextView tv_tag_name;

        public ViewHolder_latest(View itemView) {

            super(itemView);

            tv_tag_name = itemView.findViewById(R.id.tv_tag_name);
            iv_selected_row = itemView.findViewById(R.id.iv_selected_row);


        }
    }


    public void filterList(ArrayList<TagModel> filterdNames) {
        arrayList = filterdNames;
        notifyDataSetChanged();
    }


}
