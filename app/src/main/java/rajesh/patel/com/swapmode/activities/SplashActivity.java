package rajesh.patel.com.swapmode.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import rajesh.patel.com.swapmode.R;


/**
 * Created by user on 2/15/2018.
 */

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 500;
    Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=SplashActivity.this;


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               Intent intent=new Intent(context,LoginActivity.class);
               startActivity(intent);

                /*if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL)) {
                    Intent mainIntent = new Intent(SplashActivity.this, HomeLoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, UserHomeActivity.class);
                    startActivity(mainIntent);
                    finish();
                }*/


            }
        }, SPLASH_TIME_OUT);
    }
}
