package rajesh.patel.com.swapmode.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import rajesh.patel.com.swapmode.R;

public class SignUPActivity extends AppCompatActivity {
    TextView tv_have_account;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        tv_have_account=findViewById(R.id.tv_have_account);

        tv_have_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUPActivity.this,LoginActivity.class));
                finish();
            }
        });

    }
}
