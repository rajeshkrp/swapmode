package rajesh.patel.com.swapmode.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import rajesh.patel.com.swapmode.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_create_account,tv_term_and_c,tv_login_google,tv_login_fb;
    Context mContext;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


mContext=LoginActivity.this;
        tv_term_and_c=findViewById(R.id.tv_term_and_c);
        tv_create_account=findViewById(R.id.tv_create_account);
        tv_login_fb=findViewById(R.id.tv_login_fb);
        tv_login_google=findViewById(R.id.tv_login_google);


        tv_create_account.setOnClickListener(this);
        tv_login_google.setOnClickListener(this);
        tv_login_fb.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_create_account:

                startActivity(new Intent(mContext,SignUPActivity.class));
                finish();
                break;

            case R.id.tv_login_google:
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
                break;

            case R.id.tv_login_fb:
                startActivity(new Intent(mContext,DashBoardActivity.class));
finish();
                break;


        }
    }

}
