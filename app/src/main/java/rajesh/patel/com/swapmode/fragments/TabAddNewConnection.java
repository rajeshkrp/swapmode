package rajesh.patel.com.swapmode.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import rajesh.patel.com.swapmode.R;
import rajesh.patel.com.swapmode.adapters.TagListAdapter;
import rajesh.patel.com.swapmode.model.TagModel;

public class TabAddNewConnection extends Fragment {
    RecyclerView rcy_add_new;
    Context context;

    private TagListAdapter mAdapter;
    private ArrayList<TagModel> mSectionList;
    private String[] mCountries;

    TagListAdapter tagListAdapter;
    RelativeLayout layoutBottomSheet;
    ArrayList<TagModel> tagList;
    TagModel tagModel;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_add_new_connection, container, false);
        context=getActivity();
        rcy_add_new=view.findViewById(R.id.rcy_add_new);

/*

        mCountries = getApplicationContext().getResources().getStringArray(R.array.countries);
        ArrayList<CountriesModel> countriesModels = new ArrayList<>();
        for (int j = 0; j < mCountries.length ; j++) {
            countriesModels.add(new CountriesModel(mCountries[j].toString(),false));
        }
*/




        tagListData();

        return view;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }



    private void tagListData() {

        tagList=new ArrayList<>();
        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C++");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("java");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Python");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Android");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("ios");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("Php");
        tagList.add(tagModel);

        tagModel=new TagModel();
        tagModel.setTagName("C");
        tagList.add(tagModel);


        tagListAdapter=new TagListAdapter(tagList,context);
       // getHeaderListLatter(tagModel);
        rcy_add_new.setHasFixedSize(true);
        rcy_add_new.setLayoutManager(new LinearLayoutManager(context));
        rcy_add_new.setAdapter(tagListAdapter);



    }

  /*  private void getHeaderListLatter(ArrayList<TagModel> usersList) {

        Collections.sort(usersList, new Comparator<TagModel>() {
            @Override
            public int compare(TagModel user1, TagModel user2) {
                return String.valueOf(user1.getTagName()).toUpperCase().compareTo(String.valueOf(user2.getTagName().charAt(0)).toUpperCase());
            }
        });

        String lastHeader = "";

        int size = usersList.size();

        for (int i = 0; i < size; i++) {

            TagModel user = usersList.get(i);
            String header = String.valueOf(user.getTagName().charAt(0)).toUpperCase();

            if (!TextUtils.equals(lastHeader, header)) {
                lastHeader = header;
                mSectionList.add(new TagModel(header,true));
            }

            mSectionList.add(user);
        }
    }
*/



}
